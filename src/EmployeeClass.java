import models.Employee;

public class EmployeeClass{
    public static void main(String[] args) throws Exception {
        Employee nhanVien1 = new Employee(1, "Loc", "Tan", 10000000);
        Employee nhanVien2 = new Employee(2, "Cam", "My", 20000000);

        System.out.println("Kết quả của nhân viên thứ nhất là: ");
        System.out.println("toString: " + nhanVien1.toString());
        System.out.println("Lương 1 năm của nhân viên thứ nhất là: " + nhanVien1.getAnnualSalary());
        System.out.println("Tiền lương tăng của nhân viên thứ nhất là: " + nhanVien1.raiseSalary(15));


        System.out.println("Kết quả của nhân viên thứ hai là: ");
        System.out.println("toString: " + nhanVien2.toString());
        System.out.println("Lương 1 năm của nhân viên thứ hai là: " + nhanVien2.getAnnualSalary());
        System.out.println("Tiền lương tăng của nhân viên thứ hai là: " + nhanVien1.raiseSalary(15));
    }
}
