package models;

public class Employee {
     private int id;
     private String firstName;
     private String Lastname;
     private int salary;

     public Employee(int id, String firstName, String lastname, int salary) {
          this.id = id;
          this.firstName = firstName;
          Lastname = lastname;
          this.salary = salary;
     }

     public int getId() {
          return id;
     }

     public String getFirstName() {
          return firstName;
     }

     public String getLastname() {
          return Lastname;
     }

     public int getSalary() {
          return salary;
     }

     public String getName(){
          return firstName + " " + Lastname;
     }

     public void setSalary(int salary) {
          this.salary = salary;
     }

     public int getAnnualSalary(){
          return salary * 12;
     }
     
     public double raiseSalary(int percent){
          return salary * 1.1;
     }

     public String toString() {
          return "Employee[id = " + this.id + ", name = " + this.firstName + " " + this.Lastname + ", Salary = " + this.salary + "]";
      }
      
}
